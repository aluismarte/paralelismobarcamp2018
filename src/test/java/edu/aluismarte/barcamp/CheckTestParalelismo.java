package edu.aluismarte.barcamp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * https://junit.org/junit5/docs/current/user-guide/
 * <p>
 * Created by AlsNor LLC on 3/25/18.
 */
class CheckTestParalelismo {

    @Test
    void test() {
        assertEquals(4, 2 + 2, "Chequeo si 2 + 2 = 4?");
    }

    @Test
    void testFail() {
        assertNotEquals(5, 2 + 2, "Chequeo si 2 + 2 = 5?");
    }
}
