package edu.aluismarte.barcamp;

import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by aluis on 10/28/18.
 */
class DataGenerator {

    private int numbersCount = 20;

    @Test
    void test() {
        int size = 500 * 1000;
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < size; i++) {
            StringBuilder line = new StringBuilder();
            List<Integer> numbers = numberList();
            shuffle(numbers);
            for (int j = 0; j < numbers.size(); j++) {
                line.append(numbers.get(j));
                if (j < numbers.size() - 1) {
                    line.append(",");
                }
            }
            stringBuilder.append(line).append("\n");
        }
        try {
            String data = stringBuilder.toString();
            File file = new File("./data.json");
            PrintWriter printWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true))));
            printWriter.println(stringBuilder.toString().substring(0, data.length() - 1));
            printWriter.flush();
            printWriter.close();
        } catch (Exception ignored) {
            System.out.println("Error al salvar");
        }
    }

    private List<Integer> numberList() {
        List<Integer> numbers = new ArrayList<>();
        for (int i = 0; i < numbersCount; i++) {
            numbers.add(i + 1);
        }
        return numbers;
    }

    private static void shuffle(List<Integer> ar) {
        Random rnd = ThreadLocalRandom.current();
        for (int i = ar.size() - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);
            int a = ar.get(index);
            ar.set(index, ar.get(i));
            ar.set(i, a);
        }
    }
}
