package edu.aluismarte.barcamp;

import edu.aluismarte.barcamp.test.TestParalelismo;

/**
 * Created by aluis on 10/28/18.
 */
public class ParalelismoBarcamp {

    public static void main(String[] args) {
        TestParalelismo testParalelismo = new TestParalelismo();
        testParalelismo.runTest();
    }
}
