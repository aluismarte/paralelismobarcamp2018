package edu.aluismarte.barcamp.test;

import edu.aluismarte.barcamp.utils.Constants;
import edu.aluismarte.barcamp.utils.Counter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * Created by aluis on 10/28/18.
 */
public class TestParalelismo {

    private static final Counter monoCounter = new Counter();
    private static final Counter multiCounter = new Counter();

    private final List<String> DATA = new ArrayList<>();

    public TestParalelismo() {
        System.out.println("Cargando los datos");
        DATA.addAll(Constants.get().loadData());
        int time = 4;
        System.out.println("Carga terminada esperando: " + time + " segundos");
        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void runTest() {
        monoThredTest();
        System.out.println("--------------------------------------------------------");
        multiThreadTest();
    }

    private void monoThredTest() {
        long initTime = System.currentTimeMillis();
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future<Boolean> mono = executorService.submit(() -> {
            try {
                for (String data : DATA) {
                    monoCounter.sumNumberSyncronize(data);
                }
            } catch (Exception ignored) {
                System.out.println("Revisar la excepción");
                return false;
            }
            return true;
        });
        try {
            if (mono.get()) {
                System.out.println("Exito!");
                monoCounter.printResults(initTime);
            } else {
                System.out.println("Error al calcular");
            }
        } catch (Exception ignored) {
            System.out.println("No pude esperar el hilo");
        }
        executorService.shutdown();
    }

    private void multiThreadTest() {
        long initTime = System.currentTimeMillis();
        int poolSize = 5; // Siempre debe ser un multiplo de 5,000,000
        ExecutorService executorService = Executors.newFixedThreadPool(poolSize);
        List<Future<Boolean>> futures = new ArrayList<>();
        int part = DATA.size() / poolSize;
        // Divido la data en partes iguales sobre cada hilo
        for (int i = 0; i < poolSize; i++) {
            int down = i * part;
            int top = (i + 1) * part;
            futures.add(addThreadMulti(executorService, down, top));
        }
        try {
            // Obligo a terminar los hilos
            for (Future<Boolean> future : futures) {
                future.get();
            }
            System.out.println("Exito!");
            multiCounter.printResults(initTime);
        } catch (Exception ignored) {
            System.out.println("No pude esperar el hilo");
        }
        executorService.shutdown();
    }

    private Future<Boolean> addThreadMulti(ExecutorService executorService, int from, int to) {
        return executorService.submit(() -> {
            try {
                for (String data : DATA.subList(from, to)) {
                    multiCounter.sumNumberMinimalSyncronize(data);
                }
            } catch (Exception ignored) {
                System.out.println("Revisar la excepción");
                return false;
            }
            return true;
        });
    }
}
