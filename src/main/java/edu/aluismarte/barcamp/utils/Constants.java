package edu.aluismarte.barcamp.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by aluis on 10/28/18.
 */
public class Constants {

    private static volatile Constants instance = null;

    private Constants() {
    }

    public static Constants get() {
        Constants result = instance;
        if (result == null) {
            synchronized (Constants.class) {
                if (instance == null) {
                    instance = result = new Constants();
                }
            }
        }
        return result;
    }

    public List<String> loadData() {
        try {
            File file = new File(getClass().getResource("/data.json").getFile());
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            List<String> data = new ArrayList<>();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                data.addAll(Arrays.asList(line.split(",")));
            }
            return Collections.unmodifiableList(data);
        } catch (Exception ignored) {
            return new ArrayList<>();
        }
    }
}
