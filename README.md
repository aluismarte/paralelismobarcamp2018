Demo Paralelismo Barcamp 2018

# Nota
Tener cuidado con el synchronized, pues el resultado en multi hilo se puede ver enormemente afectado

# Descripción de la DATA
El archivo data.json en la carpeta resource de proyecto contiene 500,000 filas de los numeros del 1 al 20 concatenados con ",". El tamaño total del archivo son 5,000,000 de numeros y estos se repiten 500,000 veces cada uno.
El tamaño del conjunto de data afecta el resultado, debido a que si el archivo no contiene suficientes casos el proceso en mono hilo será más rápido.

# Unit Test
En los test esta el algoritmo utilizado para crear el archivo de data.

# Test
- La data siempre esta en memoria (Podemos fragmentarla y cargala a necesidad para no agotar la RAM)
- La data se divide en el test de multiples hilos en partes iguales y debe usarse un multiplo del tamaño de la data para no causar un excepción.
- Se usa un Contador con tecnica de Hash (Algo parecido a un librero)
- Existen 2 formas de sumar en el Contador tipo Hash y entregan mejor o peor rendimiento según el caso. (Probar y comprar resultados)


Ing. Aluis Marte


